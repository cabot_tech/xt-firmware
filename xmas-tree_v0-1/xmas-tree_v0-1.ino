/*
--------------------------------------------------------------------------------

Project:        XMAS TREE
Version:        0.1 - 20171112
Author:         Darian Cabot
Copyright:      (c) 2017 Cabot Technologies
License:        MIT License (see LICENSE.txt)

--------------------------------------------------------------------------------

REQUIREMENTS:

Hardware:
* Arduino - I used a Nano v3.0 (ATmega328).
* Tronixlabs WS2812B RGB LED Matrix Board - 4 x 4.
* Freetronics Microphone Sound Input Module.

Software:
* NeoPixelBus by Makuna - availble in the Arduino IDE's Library Manager.

--------------------------------------------------------------------------------

NOTES:

Ardunio sketch for testing WS2812B LEDs and microphone input. This displays the
SPL (microphone volume) on the LEDs. It continuously auto-ranges to ensure the
display is dynamic in loud or quiet environments. Upon sudden loud sound, the
LEDs will momentarily show a scrambled colour effect.

The WS2812B board is being powered by an external 5V supply (not from the
arduino board), as it probably requires more current than the Arduino is
designed to deliver when all LEDs are at full brightness.

The Mic board is very erratic when powered by 5V. This was solved by powering
with 3.3V from the arduino board instead.

--------------------------------------------------------------------------------

VISIT US:

Web:        https://cabottechnologies.com/projects/christmas-tree/
Git:        https://bitbucket.org/cabot_tech/xt-firmware
Twitter:    https://twitter.com/cabot_tech

--------------------------------------------------------------------------------
 */

// Library: 'NeoPixelBus by Makuna'.
#include <NeoPixelBus.h>

#define PixelPin 2  // Connect pin D2 to the WS2812B LED matrix data input.
#define splSensor A0 // Connect pin A0 to the microphone modules's SPL output.

#define PixelCount 16 // Make sure to set this to the number of pixels in your strip.


// LED pixel order (uncomment one and set correct PixelCount above):

// 16 LEDs - Bottom to top:
const int pixels[PixelCount] = {15, 13, 14, 12, 11, 9, 10, 8, 7, 5, 6, 4, 3, 1, 2, 0};

// 16 LEDs - Left to right:
//const int pixels[PixelCount] = {15, 14, 13, 12, 8, 9, 10, 11, 7, 6, 5, 4, 0, 1, 2, 3};

// 13 LEDs (for xmas tree) - Top star with branches bottom to top:
//const int pixels[PixelCount] = {3, 0, 6, 7, 12, 5, 1, 11, 8, 2, 4, 9, 10};


// Brightness setting for all LEDs:
// 0.5f is brightest without desaturating, but also uses the most current.
const float brightness = 0.25f;

RgbColor black(0);
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(PixelCount, PixelPin);


int splCalHighSamplesMax = 50;
int splCalHighSamples = splCalHighSamplesMax;
float splCalLow = 1.0f;
float splCalHigh = 10.0f;
float splCalSpan = 9.0f;
float splPrevHigh = 10.0f;
float splPrevLow = 1.0f;
float prevLevel = 0.0f;
float hue = 0.0f;


void setup() {
//  Serial.begin(115200); // For debug.

  strip.Begin();
  strip.Show();
}

void loop() {
  // Read current SPL from mic module and apply offset.
  float spl = analogRead(splSensor) - splCalLow;
  spl = max(spl, 0.0f); // Ensure SPL isn't negative.

  // Apply smoothing (not average, we want fast reaction to peaks).
  float splSmooth = max(splPrevHigh, spl); // Fast attack.
  splPrevHigh = splSmooth * 0.9f; // Slow decay.


  // The follow section is to find the min and max with some smoothing.
  // This allows us to auto-range the pixel display so it looks dynamic.
  
  // Adjust SPL low level...
  if (splSmooth < splCalLow)
  {
    splCalLow = splCalLow - 0.02f;
    splCalLow = max(splCalLow, 0.0f);
  } else {
    splCalLow = splCalLow + 0.01f;
  }

  // Adjust SPL high level...
  if (splSmooth > splCalHigh) {
    splCalHigh = splCalHigh + 0.5f;
    splCalSpan = splCalHigh - splCalLow;
  } else {
    splCalHigh = splCalHigh - 0.05f;
    splCalHigh = max(splCalLow + 0.1f, splCalHigh);
    splCalSpan = splCalHigh - splCalLow;
  }

  
  float level = ((float) PixelCount / splCalSpan) * spl; // Calculate current 'level' for display.
  float smoothLevel = max(prevLevel, level); // Smoothing with rapid attack.
  prevLevel = smoothLevel * 0.95f; // Slow decay.
  long levelPixels = round(smoothLevel); // Round for pixels.

  // Slowly adjust hue for colour changing effect.
  hue = hue + 0.001f;
  if (hue >= 1.0f) {
    hue = 0.0; // Overflow.
  }

  
  // Debug output for tuning:
//  Serial.print("spl:         ");
//  Serial.println(spl, DEC);
//  Serial.print("splSmooth:   ");
//  Serial.println(splSmooth, DEC);
//  Serial.print("splCalLow:   ");
//  Serial.println(splCalLow, DEC);
//  Serial.print("splCalSpan:  ");
//  Serial.println(splCalSpan, DEC);
//  Serial.print("level:       ");
//  Serial.println(level, DEC);
//  Serial.print("splCalSpan:  ");
//  Serial.println(smoothLevel, DEC);
//  Serial.print("levelPixels: ");
//  Serial.println(levelPixels, DEC);
//  Serial.print("hue:         ");
//  Serial.println(hue, DEC);
//  Serial.println();


  // Determine LED pixel colours:
  // If level is very high (sudden loud sound), then scramble all pixel hues.
  if (levelPixels > PixelCount * 3)
  {
    // All pixels on.
    for (uint16_t i = 0; i < PixelCount; i++) {
      hue = random(0, 100) / 100.0f;
      strip.SetPixelColor(pixels[i], HslColor(hue, 1.0f, brightness));
    }
  } else {
    // Set colour for each LED pixel based on current level.
    for (uint16_t i = 0; i < PixelCount; i++) {
      if (i > levelPixels) {
        // Pixel off.
  //      Serial.print("."); // Debug output.
        strip.SetPixelColor(pixels[i], black);
      } else {
        // Pixel on.
  //      Serial.print("#"); // Debug output.
        strip.SetPixelColor(pixels[i], HslColor(hue, 1.0f, brightness));
      }
    }
  }
  
  strip.Show(); // Update LED pixels.
  delay(10);
}

