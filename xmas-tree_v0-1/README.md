# XMAS TREE
Version:        0.1 - 20171112
Author:         Darian Cabot
Copyright:      (c) 2017 Cabot Technologies
License:        MIT License (see LICENSE.txt)

## Requirements

### Hardware

* Arduino - I used a Nano v3.0 (ATmega328).
* [Tronixlabs WS2812B RGB LED Matrix Board - 4 x 4](https://tronixlabs.com.au/components/leds/ws2812b/ws2812b-rgb-led-matrix-board-4-x-4-australia/)
* [Freetronics Microphone Sound Input Module](https://www.freetronics.com.au/products/microphone-sound-input-module)

###Software

* NeoPixelBus by Makuna - availble in the Arduino IDE's Library Manager.

## Notes

Ardunio sketch for testing WS2812B LEDs and microphone input. This displays the SPL (microphone volume) on the LEDs. It continuously auto-ranges to ensure the display is dynamic in loud or quiet environments. Upon sudden loud sound, the LEDs will momentarily show a scrambled colour effect.

The WS2812B board is being powered by an external 5V supply (not from the arduino board), as it probably requires more current than the Arduino is designed to deliver when all LEDs are at full brightness.

The Mic board is very erratic when powered by 5V. This was solved by powering with 3.3V from the arduino board instead.

## Visit us

* Web: https://cabottechnologies.com/projects/christmas-tree/
* Git: https://bitbucket.org/cabot_tech/xt-firmware
* Twitter: https://twitter.com/cabot_tech
